package org.servlet;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.logic.api.model.DAOProvider;
import org.logic.api.model.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("v1/departments/")
public class DepartmentRouter {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(DepartmentRouter.class);

	@GET
	@Produces(APPLICATION_JSON)
	public List<Department> getDepartments() throws SQLException {
		return DAOProvider.departmentDAO().queryForAll();
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	public Response createRootDepartment(String name) {
		if (name.length() > 0) {
			Department department;
			try {
				department = getDepartmentByNameOrThrow404(name);
			} catch (WebApplicationException e) {
				department = new Department(name);
			}
			try {
				DAOProvider.departmentDAO().create(department);
			} catch (SQLException e) {
				throw new WebApplicationException(e.getMessage(), Response.Status.fromStatusCode(500));
			}
			return Response.ok(department, APPLICATION_JSON).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).entity("Department name length must be greater than 0")
					.build();
		}
	}

	@GET
	@Produces(APPLICATION_JSON)
	@Path("name/{name}")
	public Response getDepartment(@PathParam("name") String name) {
		Department department;
		department = getDepartmentByNameOrThrow404(name);
		return Response.ok(department, APPLICATION_JSON).build();
	}

	@GET
	@Produces(APPLICATION_JSON)
	@Path("{id}")
	public Response getDepartment(@PathParam("id") int id) {
		Department department;
		department = getDepartmentById(id);
		if (department != null) {
			return Response.ok(department, APPLICATION_JSON).build();
		} else {
			throw new WebApplicationException("Department " + id + " does not exist", Response.Status.NOT_FOUND);
		}
	}

	@POST
	@Produces(APPLICATION_JSON)
	@Path("{id}")
	public Response createChildDepartment(@PathParam("id") int parentId, String childName) {
		Department parent = getDepartmentById(parentId);
		Department child = getDepartmentByName(childName);
		if (child != null) {
			return Response.status(Response.Status.BAD_REQUEST)
					.entity("Child department with this name exists, it should be moved instead of creating").build();
		} else {
			child = new Department(childName);
			try {
				DAOProvider.departmentDAO().create(child);
				child.setParent(parent);
				DAOProvider.departmentDAO().update(child);
				DAOProvider.departmentDAO().update(parent);
				return Response.ok().entity(child).build();
			} catch (SQLException e) {
				throw new WebApplicationException(e.getMessage(), Response.Status.fromStatusCode(500));
			}
		}
	}

	@GET
	@Path("{id}/children")
	public List<Department> getDepartmentChildren(@PathParam("id") int id) {
		return getDepartmentById(id).getChildDepartments().stream().map(childId -> getDepartmentById(childId.getId()))
				.collect(Collectors.toList());
	}

	@PUT
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("{id}/rename")
	public Response renameDepartment(@PathParam("id") int id, String newName) {
		Department department = getDepartmentById(id);
		if (getDepartmentByName(newName) != null) {
			return Response.status(Response.Status.CONFLICT).build();
		}
		if (department != null) {
			try {
				department.setName(newName);
				DAOProvider.departmentDAO().update(department);
				return Response.ok(department).build();
			} catch (SQLException e) {
				throw new WebApplicationException(e.getMessage(), Response.Status.fromStatusCode(500));
			}
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@DELETE
	@Produces(APPLICATION_JSON)
	@Path("{id}")
	public Response deleteDepartment(@PathParam("id") int id) {
		Department department;
		try {
			department = DAOProvider.departmentDAO().queryForId(id);
			if (department != null) {
				if (department.getChildDepartments().size() == 0) {
					if (department.getHeadDepartment() != null) {// otherwise
																	// its
						// a root
						Department parent = getDepartmentById(department.getHeadDepartment().getId());
						parent.removeChildDepartment(department);
						DAOProvider.departmentDAO().update(parent);
					}

					DAOProvider.departmentDAO().delete(department);
					return Response.ok().build();
				} else {
					return Response.status(Response.Status.BAD_REQUEST)
							.entity("Department can't be deleted until all its' children will be either deleted or moved to other parent")
							.build();
				}
			} else {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} catch (SQLException e) {
			throw new WebApplicationException(e.getMessage(), Response.Status.fromStatusCode(500));
		}
	}

	@PUT
	@Path("{id}")
	public Response moveDepartment(int newParentId, @PathParam("id") int id) {
		Department department = getDepartmentById(id);
		Department newParent = getDepartmentById(newParentId);

		Department oldParent = getDepartmentById(department.getHeadDepartment().getId());
		department.moveBetweenParents(oldParent, newParent);

		try {
			DAOProvider.departmentDAO().update(department);
			DAOProvider.departmentDAO().update(newParent);
			DAOProvider.departmentDAO().update(oldParent);
		} catch (SQLException e) {
			throw new WebApplicationException(e.getMessage(), Response.Status.fromStatusCode(500));
		}

		return Response.ok().entity(department).build();
	}

	private Department getDepartmentByName(String name) {
		try {
			List<Department> list = DAOProvider.departmentDAO().queryForEq("name", name);
			return list.size() > 0 ? list.get(0) : null;
		} catch (SQLException e) {
			throw new WebApplicationException(e.getMessage(), Response.Status.fromStatusCode(500));
		}
	}

	private Department getDepartmentByNameOrThrow404(String name) {
		try {
			List<Department> list = DAOProvider.departmentDAO().queryForEq("name", name);
			if (list.size() == 0) {
				throw new WebApplicationException(Response.Status.NOT_FOUND);
			}
			return list.get(0);
		} catch (SQLException e) {
			throw new WebApplicationException(e.getMessage(), Response.Status.fromStatusCode(500));
		}
	}

	private Department getDepartmentById(int id) {
		try {
			Department department = DAOProvider.departmentDAO().queryForId(id);
			if (department == null) {
				throw new WebApplicationException(Response.Status.NOT_FOUND);
			}
			return department;
		} catch (SQLException e) {
			throw new WebApplicationException(e.getMessage(), Response.Status.fromStatusCode(500));
		}
	}

}
