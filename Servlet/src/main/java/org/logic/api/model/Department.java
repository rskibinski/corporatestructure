package org.logic.api.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "DEPARTMENT")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Department {

	@Id
	@Column(name = "DEPARTMENT_ID")
	@GeneratedValue
	private Integer id;

	@Column(name = "NAME")
	private String name;

	@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Department headDepartment;

	@OneToMany(mappedBy = "headDepartment", fetch = FetchType.EAGER)
	private Set<Department> childDepartments = new HashSet<>();

	Department() {
	}

	public Department(String name) {
		this.name = name;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department getHeadDepartment() {
		return headDepartment;
	}

	public void setHeadDepartment(Department headDepartment) {
		this.headDepartment = headDepartment;
	}

	public Set<Department> getChildDepartments() {
		return childDepartments;
	}

	public void addChildDepartment(Department child) {
		child.setHeadDepartment(this);
		childDepartments.add(child);
	}

	public void setParent(Department parent) {
		setHeadDepartment(parent);
		parent.getChildDepartments().add(this);
	}

	public void removeChildDepartment(Department child) {
		if (childDepartments.contains(child)) {
			child.setHeadDepartment(child);
			childDepartments.remove(child);
		}
	}

	public void moveBetweenParents(Department oldParent, Department newParent) {
		oldParent.removeChildDepartment(this);
		newParent.addChildDepartment(this);
	}
}
