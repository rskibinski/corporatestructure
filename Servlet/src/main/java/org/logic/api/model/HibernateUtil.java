package org.logic.api.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static final SessionFactory sessionFactory;
	
	static{
		sessionFactory = new Configuration().configure().buildSessionFactory();
	}
	
	public static Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public static void closeSession(){
		sessionFactory.getCurrentSession().close();
	}
	
}
