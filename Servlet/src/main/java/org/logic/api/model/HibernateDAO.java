package org.logic.api.model;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

public class HibernateDAO<T, K extends Serializable> implements GenericDAO<T, K> {

	private final Class<T> entityClass;

	public HibernateDAO(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public void create(T element) throws SQLException {
		Session session = HibernateUtil.getSession();
		Transaction tx = session.beginTransaction();
		session.save(element);
		tx.commit();
		HibernateUtil.closeSession();
	}

	@Override
	public void update(T element) throws SQLException {
		Session session = HibernateUtil.getSession();
		Transaction tx = session.beginTransaction();
		session.update(element);
		tx.commit();
		HibernateUtil.closeSession();
	}

	@Override
	public void delete(T element) throws SQLException {
		Session session = HibernateUtil.getSession();
		Transaction tx = session.beginTransaction();
		session.delete(element);
		tx.commit();
		HibernateUtil.closeSession();
	}

	@Override
	public List<T> queryForAll() throws SQLException {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		Query<T> query = session.createQuery("from " + entityClass.getSimpleName(), entityClass);
		List<T> results = query.stream().collect(Collectors.toList());
		HibernateUtil.closeSession();
		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> queryForEq(String fieldName, String value) throws SQLException {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		@SuppressWarnings("deprecation")
		List<T> results = session.createCriteria(entityClass)
		.add(Restrictions.eq(fieldName, value)).list();
		HibernateUtil.closeSession();
		return results;
	}

	@Override
	public T queryForId(K id) throws SQLException {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		T object = session.get(entityClass, id);
		HibernateUtil.closeSession();
		return object;
	}

}
