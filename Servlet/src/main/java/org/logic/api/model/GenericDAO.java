package org.logic.api.model;

import java.sql.SQLException;
import java.util.List;

public interface GenericDAO<T, K> {

	void create(T element) throws SQLException;

	void update(T element) throws SQLException;

	void delete(T element) throws SQLException;

	List<T> queryForAll() throws SQLException;

	List<T> queryForEq(String fieldName, String value) throws SQLException;

	T queryForId(K id) throws SQLException;

}
