package org.logic.api.model;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DAOProvider {

	@SuppressWarnings("unused")
	private final static Logger LOG = LoggerFactory.getLogger(DAOProvider.class);

	private static GenericDAO<Department, Integer> DEPARTMENT_DAO = new HibernateDAO<>(Department.class);

	static {
		try {
			populate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public DAOProvider() {
	}

	public static GenericDAO<Department, Integer> departmentDAO() {
		return DEPARTMENT_DAO;
	}

	private static void populate() throws SQLException {
		Department root = new Department("Root department");
		DEPARTMENT_DAO.create(root);
		Department d1 = new Department("Child department");
		Department d2 = new Department("Child department 2");
		DEPARTMENT_DAO.create(d1);
		DEPARTMENT_DAO.create(d2);
		d1.setParent(root);
		d2.setParent(root);

		Department d3 = new Department("Grandchild 1");
		Department d4 = new Department("Grandchild 2");
		Department d5 = new Department("Grandchild 3");
		DEPARTMENT_DAO.create(d3);
		DEPARTMENT_DAO.create(d4);
		DEPARTMENT_DAO.create(d5);
		d3.setParent(d1);
		d4.setParent(d1);
		d5.setParent(d2);

		DEPARTMENT_DAO.update(root);
		DEPARTMENT_DAO.update(d1);
		DEPARTMENT_DAO.update(d2);
		DEPARTMENT_DAO.update(d3);
		DEPARTMENT_DAO.update(d4);
		DEPARTMENT_DAO.update(d5);
	}
}
