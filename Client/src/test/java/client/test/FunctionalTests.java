package client.test;

import org.junit.Assert;
import org.junit.Test;

import client.Department;
import client.TestClient;

public class FunctionalTests {

	private TestClient client = new TestClient();

	@Test
	public void testDeserialize() {
		client.getAllDepartments().forEach(d -> {
			d.getName(); // not really a test. just expect a npe if something
							// will be wrong.
		});
	}

	@Test
	public void testCreate() {
		int size = client.getAllDepartments().size();
		Department department = client.createDepartment("Test root").get();
		Assert.assertEquals(size + 1, client.getAllDepartments().size());
		client.deleteDepartment(department);
	}

	@Test
	public void testDelete() {
		int size = client.getAllDepartments().size();
		Department department = client.createDepartment("Test delete").get();
		Assert.assertEquals(size + 1, client.getAllDepartments().size());
		client.deleteDepartment(department);
		Assert.assertEquals(size, client.getAllDepartments().size());
	}

	@Test
	public void testMove() {
		Department g3 = client.getDepartment(6).get();
		int oldParentId = g3.getHeadDepartment().getId();

		Department newParent = client.getDepartment(2).get();
		g3 = client.moveDepartment(g3, newParent).get();
		newParent = client.getDepartment(2).get();

		Department oldParent = client.getDepartment(oldParentId).get();
		Assert.assertFalse(oldParent.getChildDepartments().contains(g3));
		Assert.assertTrue(g3.getHeadDepartment().getId() == (newParent.getId()));
		Assert.assertTrue(newParent.getChildDepartments().contains(g3));

		client.moveDepartment(g3, oldParent);
	}

	@Test
	public void testRename() {
		Department g3 = client.getDepartment(6).get();
		String oldName = g3.getName();
		String newName = "ANOTHER GRANDCHILD";

		Department department = client.rename(g3, newName).get();
		Assert.assertTrue(department.getName().equals(newName));

		client.rename(g3, oldName);
	}

}
