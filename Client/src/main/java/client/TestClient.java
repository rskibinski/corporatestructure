package client;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status.Family;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestClient {
	private final static Logger log = LoggerFactory.getLogger(TestClient.class);

	private Client client;

	private final String URI = "http://localhost:8080/Servlet/v1/departments"; // TODO
	// injection

	public TestClient() {
		client = ClientBuilder.newClient();

		HttpAuthenticationFeature authFeature = HttpAuthenticationFeature.basic("rest", "rest2");
		client.register(authFeature);

	}

	public List<Department> getAllDepartments() {
		WebTarget target = webTarget();
		Response response = target.request(APPLICATION_JSON).get();
		List<Department> readEntity = response.readEntity(new GenericType<List<Department>>() {
		});
		return readEntity;
	}

	public Optional<Department> getDepartment(int name) {
		WebTarget target = webTarget(String.valueOf(name));
		Response response = target.request(APPLICATION_JSON).get();
		Department department = null;
		if (Response.Status.fromStatusCode(response.getStatus()) == Response.Status.OK) {
			department = response.readEntity(Department.class);
		}
		return Optional.ofNullable(department);
	}

	public Optional<Department> createDepartment(String name) {
		WebTarget target = webTarget();
		Response response = target.request(APPLICATION_JSON).post(Entity.json(name));
		return getDepartmentIfResponseSuccessful(response);

	}

	public Optional<Department> createDepartment(Department parent, String name) {
		WebTarget target = webTarget(String.valueOf(parent.getId()));
		Response response = target.request(APPLICATION_JSON).post(Entity.json(name));
		return getDepartmentIfResponseSuccessful(response);
	}

	public Optional<Department> moveDepartment(Department department, Department newParent) {
		WebTarget target = webTarget(String.valueOf(department.getId()));
		Response response = target.request(APPLICATION_JSON).put(Entity.json(newParent.getId()));
		return getDepartmentIfResponseSuccessful(response);
	}

	public Optional<Department> rename(Department department, String newName) {
		WebTarget target = webTarget(String.valueOf(department.getId()), "rename");
		Response response = target.request(APPLICATION_JSON).put(Entity.json(newName));
		return getDepartmentIfResponseSuccessful(response);
	}

	private Optional<Department> getDepartmentIfResponseSuccessful(Response response) {
		Department department = null;
		if (response.getStatusInfo().getFamily().equals(Family.SUCCESSFUL)) {
			department = response.readEntity(Department.class);
		}
		return Optional.ofNullable(department);
	}

	public void deleteDepartment(Department department) {
		WebTarget target = webTarget(String.valueOf(department.getId()));
		Response response = target.request().delete();
		log.debug(response.getStatus() + "");
	}

	private WebTarget webTarget(String... paths) {
		WebTarget target = client.target(URI);
		for (String path : paths) {
			target = target.path(path);
		}
		return target;
	}
}
