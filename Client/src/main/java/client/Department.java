package client;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Department {

	private Integer id;

	private String name;

	private Department headDepartment;

	private Set<Department> childDepartments = new HashSet<>();

	Department() {
	}

	public Department(String name) {
		this.name = name;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department getHeadDepartment() {
		return headDepartment;
	}

	public void setHeadDepartment(Department headDepartment) {
		this.headDepartment = headDepartment;
	}

	public Set<Department> getChildDepartments() {
		return childDepartments;
	}

	public void addChildDepartment(Department child) {
		child.setHeadDepartment(this);
		childDepartments.add(child);
	}

	public void setParent(Department parent) {
		setHeadDepartment(parent);
		parent.getChildDepartments().add(this);
	}

	public void removeChildDepartment(Department child) {
		if (childDepartments.contains(child.getId())) {
			child.setHeadDepartment(child);
			childDepartments.remove(child.getId());
		}
	}

	public void moveBetweenParents(Department oldParent, Department newParent) {
		oldParent.removeChildDepartment(this);
		newParent.addChildDepartment(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!Department.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		final Department other = (Department) obj;
		return this.getId() == other.getId();
	}

	@Override
	public int hashCode() {
		int h = 3;
		h = 53 * h + (this.id != null ? this.id.hashCode() : 0);
		return h;
	}
}
